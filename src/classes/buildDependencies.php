<?php
namespace FAE\fae;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class buildDependencies extends Command {

  function __construct(){
    global $config;
    $this->defaults = (object) [
      'composer' => [
        'require'           => [],
        'autoload'          => [ 'psr-4' => [ $config->namespace.'\\' => APPROOT . DIRECTORY_SEPARATOR . 'src/classes/' ] ],
        'minimum-stability' => 'dev',
        'prefer-stable'     => true,
      ]
    ];
    parent::__construct();
  }
  
  protected static $defaultName = 'app:build-dependencies';

  function execute(InputInterface $input, OutputInterface $output){
    $composer = (object) $this->defaults->composer;
    $fh = new frames();
    $frames = $fh->loadFrames();
    foreach( $frames as $frame => $options ){
      
      if( isset( $options->composer )){
        $composer->require = array_merge( (array) $composer->require, (array) $options->composer->require );
        if( isset( $options->composer->autoload )){
          foreach( $options->composer->autoload as $loader => $classes ){
            foreach( $classes as $class => $path ){
              $options->composer->autoload->{$loader}->{$class} = implode( DIRECTORY_SEPARATOR, [ APPROOT, 'frames', $frame, $path ] );
            }
          }
          $composer->autoload = array_merge_recursive( (array ) $composer->autoload, (array) $options->composer->autoload );
        }
      }
      
    }
    
    $output->writeln(json_encode( $composer, JSON_UNESCAPED_SLASHES ));
  }
  
}