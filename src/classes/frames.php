<?php

namespace FAE\fae;

use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\Cache\Exception\LogicException;

use FAE\cache\cache_adapter;

use stdClass;

class frames
{

  var $frames;
  var $framePaths;

  static $_frameCache;

  /**
   * Initialise frame class defining framepaths
   *
   * @param array $framePaths
   */
  function __construct(?array $framePaths = null)
  {
    global $config;
    if($framePaths){
      $this->framePaths = $framePaths;
    } elseif($config->framePaths){
      $this->framePaths = $config->framePaths;
    } else {
      $this->framePaths = [APPROOT . '/vendor/fae'];
    }
  }

  /**
   * Load an array of all available frames
   * 
   * Utilises low level cache 
   *
   * @return array
   */
  function loadFrames( ?string $frame = null )
  {
    if (self::$_frameCache) {
      return self::$_frameCache;
    }
    $cacheAdapter = new cache_adapter(cache_adapter::LOW);
    $cacheInstance = $cacheAdapter->getCache();
    self::$_frameCache = $cacheInstance->get('frames', function (ItemInterface $item) {
      $item->expiresAfter(86400);
      try {
        $item->tag(['frames']);
      } catch (LogicException $e) {
        // ignore, as tags are not supported
      }
      return $this->loadFrameData();
    }, 5.0);
    return $frame ? self::$_frameCache[$frame] : self::$_frameCache;
  }

  /**
   * Parse frame.json files for all discovered frames within defined frame paths
   *
   * @return array
   */
  function loadFrameData()
  {

    foreach ($this->framePaths as $framePath) {
      if (!is_dir($framePath)) {
        // Do an error
        throw new \RuntimeException("Could not load frames due to missing framepath '{$framePath}'");
      }

      foreach (scandir($framePath) as $frame) {
        if (is_dir($framePath . DIRECTORY_SEPARATOR . $frame) && !in_array($frame, ['.', '..'])) {
          $frameOptions = implode(DIRECTORY_SEPARATOR, [$framePath, $frame, 'frame.json']);
          if (file_exists($frameOptions)) {
            // Parse frame.json as json into object
            $this->frames[$frame] = file_exists($frameOptions) ? json_decode(file_get_contents($frameOptions)) : new stdClass;
            // Define a path for the frame
            $this->frames[$frame]->path = implode(DIRECTORY_SEPARATOR, [$framePath, $frame]);
          }
        }
      }
    }

    return $this->frames;
  }
}
