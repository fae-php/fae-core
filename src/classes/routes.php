<?php

namespace FAE\fae;

use Symfony\Component\Routing\RouteCollection;

class routes
{

  static $_routes;

  static function addCollection(RouteCollection $collection)
  {
    if (!self::$_routes) {
      self::$_routes = new RouteCollection();
    }

    return self::$_routes->addCollection($collection);
  }

  static function getCollection()
  {
    if (!self::$_routes) {
      self::$_routes = new RouteCollection();
    }

    return self::$_routes;
  }

  static function bootstrapLoader()
  {
    $frameLoader = new frames();
    $frames = $frameLoader->loadFrames();

    foreach ($frames as $frame => $options) {
      if (is_object($options) && property_exists($options, 'fae') && property_exists($options->fae, 'routeloader')) {
        foreach ($options->fae->routeloader as $route) {
          require(implode(DIRECTORY_SEPARATOR, [$options->path, $route]));
        }
      }
    }
  }
}
