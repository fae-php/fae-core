<?php
namespace FAE\fae;

class fae {
  
  static $_hooks = [];
  static $_options;
  static $mimeTypes = [
    'txt' => 'text/plain',
    'htm' => 'text/html',
    'html' => 'text/html',
    'php' => 'text/html',
    'css' => 'text/css',
    'js' => 'application/javascript',
    'json' => 'application/json',
    'xml' => 'application/xml',
    'swf' => 'application/x-shockwave-flash',
    'flv' => 'video/x-flv',

    // images
    'png' => 'image/png',
    'jpe' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'jpg' => 'image/jpeg',
    'gif' => 'image/gif',
    'bmp' => 'image/bmp',
    'ico' => 'image/vnd.microsoft.icon',
    'tiff' => 'image/tiff',
    'tif' => 'image/tiff',
    'svg' => 'image/svg+xml',
    'svgz' => 'image/svg+xml',

    // archives
    'zip' => 'application/zip',
    'rar' => 'application/x-rar-compressed',
    'exe' => 'application/x-msdownload',
    'msi' => 'application/x-msdownload',
    'cab' => 'application/vnd.ms-cab-compressed',

    // audio/video
    'mp3' => 'audio/mpeg',
    'qt' => 'video/quicktime',
    'mov' => 'video/quicktime',

    // adobe
    'pdf' => 'application/pdf',
    'psd' => 'image/vnd.adobe.photoshop',
    'ai' => 'application/postscript',
    'eps' => 'application/postscript',
    'ps' => 'application/postscript',

    // ms office
    'doc' => 'application/msword',
    'rtf' => 'application/rtf',
    'xls' => 'application/vnd.ms-excel',
    'ppt' => 'application/vnd.ms-powerpoint',

    // open office
    'odt' => 'application/vnd.oasis.opendocument.text',
    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
  ];
  
  static function loadHooks( bool $cache = true )
  {
    if(count(self::$_hooks)){
      return self::$_hooks;
    }
    
    $framesRef = new frames();
    $frames = $framesRef->loadFrames();
    
    foreach( $frames as $frame => $options ){
      if( is_object($options) && property_exists( $options, 'hooks') ){
        foreach( $options->hooks as $type => $data ){
          self::$_hooks[$type] = array_merge_recursive( (array) $data, isset(self::$_hooks[$type]) ? (array) self::$_hooks[$type] : array() );
        }
      }
    }
    
    return self::$_hooks;
  }
  
  static function getHooks(string $namespace){
    self::loadHooks();
    return self::$_hooks[$namespace];
  }
  
  static function loadOptions( string $type, bool $cache = true )
  {
    if(is_object(self::$_options)){
      if (isset(self::$_options->{$type})) {
        return self::$_options->{$type};
      }
    }
    
    $framesRef = new frames();
    $frames = $framesRef->loadFrames();
    
    foreach( $frames as $frame => $options ){
      if( is_object($options) && property_exists( $options, 'fae') && is_object( $options->fae ) ){
        self::$_options = (object) array_merge_recursive( (array) $options->fae, (array) self::$_options );
      }
    }
    
    if (isset(self::$_options->{$type})) {
      return self::$_options->{$type};
    }

    return null;
  }
  
  static function _path(...$items){
    return str_replace( ['/','\\'], DIRECTORY_SEPARATOR, implode( DIRECTORY_SEPARATOR, $items ) );
  }
  
  static function guid()
  {
    if( function_exists('com_create_guid') === true ){
      return trim( com_create_guid(), '{}' );
    }
    return sprintf( '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) );
  }
  
  static function echofile( $filePath, $readChunk = 10485760 )
  {
    $fileSize = filesize($filePath);
    if( $readChunk && $fileSize > $readChunk ){
      $handle = fopen($filePath, 'rb');
      while (!feof($handle)) {
        echo fread($handle, $readChunk);
        @ob_flush();
        @flush();
      }
      fclose($handle);
      return $fileSize;
    }
    return readfile($filePath);
  }
  
  static function mimeType( string $file )
  {
    $value = explode(".", $file);
    $ext = strtolower(array_pop($value));
    if (array_key_exists($ext, self::$mimeTypes)) {
      return self::$mimeTypes[$ext];
    } elseif (function_exists('finfo_open')) {
      $finfo = finfo_open(FILEINFO_MIME);
      $mimetype = finfo_file($finfo, $file);
      finfo_close($finfo);
      return $mimetype;
    } else {
      return 'application/octet-stream';
    }
  }
}